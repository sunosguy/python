#!/usr/bin/python
import os
import subprocess
import string
import sys

if len(sys.argv) == 1:
	sys.exit('Usage: %s domain_name' % sys.argv[0])

xmppdomain = sys.argv[1]

def sshFunc():
	command = "(/usr/bin/sudo /bin/grep -m 1 ldap_password /opt/voalte/etc/ejabberd/ejabberd.cfg )"
	ssh = subprocess.Popen(["ssh", "%s" % xmppdomain, command],
                       shell=False,
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)
	result = ssh.stdout.readlines()
	if result == []:
   		error = ssh.stderr.readlines()
		print >>sys.stderr, "ERROR: %s" % error
	else:
		for line in result:
			fields = line.strip().split('"')
			fieldone = (fields[1]).rstrip()
			cmd = 'echo -n %s | base64 ' % str(fields[1])
    			print "Encoded String\n", fieldone 
			print "Decoded String"
			os.system(cmd) 


def webpageFunc():
	print "\nYour Voalte admin page is located at:"
	print "http://" + xmppdomain + "/voalteadmin/#login"
	print "http://" + xmppdomain + "/support"

	print "\nYour P2A  ejabberd page is located at:"
	print "http://" + xmppdomain + ':5280/admin/server/' + xmppdomain + "/node/ejabberd@localhost/modules/"
	print "\nYour P2B  ejabberd page is located at:"
	print "http://" + xmppdomain + ':5281/admin/server/' + xmppdomain + "/node/ejabberd@localhost/modules/"
	print "to validate your config after making changes"
	print "sudo erl -noshell -eval \'{ok, R} = file:consult(\"/opt/voalte/etc/ejabberd/ejabberd.cfg\"),init:stop()\'"




webpageFunc()
sshFunc()


