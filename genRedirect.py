#!/usr/bin/python
#
import sys

""" Let's grab $1 as a variable and use it as the filename to 
parse """
if len(sys.argv) == 1:
	sys.exit('Usage: %s file_to_process' % sys.argv[0])

rdfile = sys.argv[1]
mywfile = open("redirect.out","w")

#Let's take 
for line in open(rdfile,"r"):
	line = line.strip().replace("\t", "")
	if line != '':
		#let's turn the line into a string and then
		# we will strip off the whitespace
		out = str(line).split(',')
		newout = out[1].strip().replace(" ", "")

		#Let's format the line now
		redirect = "\t\t{\"" + out[0] + '"' + ',' + "\t\t\"" + \
		newout + '"},'
		# Let's write the final stripped line to the file
		mywfile.write(redirect)
		#write a newline so the file isn't all jumbled
		mywfile.write("\n")

# Let's change mywfile to read-only and 
# print the results to screen and then close the file
mywfile = open("redirect.out","r")
print mywfile.read()
mywfile.close()



print "\n\n To validate your configuration changes run the following after\n editing the ejabberd.cfg file\n"
print "sudo erl -noshell -eval \'{ok, R} = file:consult(\"/opt/voalte/etc/ejabberd/ejabberd.cfg\"),init:stop()\'"


